# BowlingCalculatorApp

this app calculates game results for a bowling game from a text file
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
You need install in your computer: 

* java 8
* maven

### Installing

Download code from repo and inside Project's root folder execute:
```
mvn compile
mvn package
```

A jar file "bowlingCalculatorApp-0.0.1-SNAPSHOT" will be generated inside target folder

## Running the tests

All test are executed in packaging

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* Spring boot - framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* Apache commons - tools for Strings and text
* Apache commons IO - tools for files
* JUnit 4.8.2 - testing

## Authors

* **Jaime Echeverry** - *All work*
 
## How to execute app

After execute mvn package you have to go or copy the generated jar file "bowlingCalculatorApp-0.0.1-SNAPSHOT"
inside target folder.

After that you have to execute the next command inside jar's folder:

```
java -jar bowlingCalculatorApp-0.0.1-SNAPSHOT.jar "c://game.txt"
```
where "c://game.txt" is the path from text file to calculate

## Aditional Notes

* this app **uses** Java8 streams and lambdas
