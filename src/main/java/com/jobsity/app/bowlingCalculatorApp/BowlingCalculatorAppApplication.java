package com.jobsity.app.bowlingCalculatorApp;

import com.jobsity.app.bowlingCalculatorApp.model.ReadDataFromFile;
import com.jobsity.app.bowlingCalculatorApp.model.object.Player;
import com.jobsity.app.bowlingCalculatorApp.model.object.RawScore;
import com.jobsity.app.bowlingCalculatorApp.service.GameResults;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import com.jobsity.app.bowlingCalculatorApp.service.CalculateGame;

import java.util.List;
import java.util.Set;

@ComponentScan("com.jobsity.app")
@SpringBootApplication
public class BowlingCalculatorAppApplication implements CommandLineRunner {

	static final Logger logger = LogManager.getLogger(BowlingCalculatorAppApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(BowlingCalculatorAppApplication.class, args);
	}

	@Autowired
	ReadDataFromFile readDataFromFile;

	@Autowired
	private CalculateGame calculateGame;

	@Autowired
	GameResults gameResults;

	@Override
	public void run(String... args) throws Exception {
		logger.info("****************************************************");
		logger.info("WELCOME TO BOWLING CALCULATOR CONSOLE APP");
		logger.info("****************************************************");

		StringBuilder result = new StringBuilder();
		String filePath = args.length > 0 ? args[0].toString() : StringUtils.EMPTY;
		List<RawScore> scores = getReadDataFromFile().getRawDataByPlayer(filePath);
		Set<Player> players = getCalculateGame().calculateDataByPlayer(scores);
		result = getGameResults().displayResults(players);
		System.out.println(result);
		logger.info("All done!");
	}

	public CalculateGame getCalculateGame() {
		return calculateGame;
	}

	public void setCalculateGame(CalculateGame calculateGame) {
		this.calculateGame = calculateGame;
	}

	public GameResults getGameResults() {
		return gameResults;
	}

	public void setGameResults(GameResults gameResults) {
		this.gameResults = gameResults;
	}

	public ReadDataFromFile getReadDataFromFile() {
		return readDataFromFile;
	}

	public void setReadDataFromFile(ReadDataFromFile readDataFromFile) {
		this.readDataFromFile = readDataFromFile;
	}
}
