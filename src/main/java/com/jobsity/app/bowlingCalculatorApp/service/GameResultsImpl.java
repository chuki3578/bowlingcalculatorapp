package com.jobsity.app.bowlingCalculatorApp.service;

import com.jobsity.app.bowlingCalculatorApp.model.object.Player;
import com.jobsity.app.bowlingCalculatorApp.model.object.Throw;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Create text for display game results in console
 */
@Service
public class GameResultsImpl implements GameResults {

    static final Logger logger = LogManager.getLogger(GameResults.class);
    private final String FRAME = "Frame";
    private final String SCORE = "Score";
    private final String PINFALLS = "Pinfalls";
    private final int MAX_PINFALLS = 4;
    private final int MAX_ROW = 8;

    public StringBuilder displayResults(Set<Player> players) {
        StringBuilder sb = new StringBuilder();

        logger.info("Removing incomplete games by player");
        players.removeIf(x->x.getThrowList().size()<10);

        if (players == null || players.isEmpty()) {
            logger.info("None data to display!");
            return sb;
        }

        sb.append("\n\n");
        int maxName = players.stream().map(Player::getName).mapToInt(String::length).max().getAsInt();
        maxName = (maxName > MAX_ROW ? maxName : MAX_ROW) + 3;
        sb.append(StringUtils.rightPad(FRAME, maxName, StringUtils.SPACE));

        for (int i = 1; i <= 10; i++) {
            sb.append(StringUtils.rightPad(String.valueOf(i), 2 * MAX_PINFALLS, StringUtils.SPACE));
        }
        sb.append("\n");

        for (Player player : players) {

            sb.append(player.getName() + "\n");
            sb.append(StringUtils.rightPad(PINFALLS, maxName, StringUtils.SPACE));
            StringBuilder score = new StringBuilder("\n");
            score.append(StringUtils.rightPad(SCORE, maxName, StringUtils.SPACE));

            for (Throw thr : player.getThrowList()) {
                printGameRows(thr, sb, score);
            }
            sb.append(score + "\n");
        }
        sb.append("\n\n");
        return sb;
    }

    /**
     * Adds row to StringBuilder calculating the correct spaces for each row
     * @param thr Throw
     * @param sb
     * @param score
     * @return text for print
     */
    private StringBuilder printGameRows(Throw thr, StringBuilder sb, StringBuilder score) {
        String first = thr.getFirstThrow();
        String second = thr.getSecondThrow() == null ? StringUtils.EMPTY : thr.getSecondThrow();
        String total = String.valueOf(thr.getTotalScore());
        score.append(StringUtils.rightPad(total, 2 * MAX_PINFALLS, StringUtils.SPACE));
        String third = thr.getNext() == null ? (thr.getThirdThrow() == null ? StringUtils.EMPTY : thr.getThirdThrow()) : null;

        if (third != null) {
            sb.append(StringUtils.rightPad(ValidateIfTen(first), MAX_PINFALLS, StringUtils.SPACE));
            sb.append(StringUtils.rightPad(ValidateIfTen(second), MAX_PINFALLS, StringUtils.SPACE));
            sb.append(ValidateIfTen(third));
        } else {
            int sum = sumThrows(first) + sumThrows(second);
            sb.append(StringUtils.rightPad(ValidateIfTen(first), MAX_PINFALLS, StringUtils.SPACE));
            sb.append(StringUtils.rightPad(sum == 10 ? "/" : second, MAX_PINFALLS, StringUtils.SPACE));
        }

        return sb;
    }

    /**
     * Converts String to number
     * @param value
     * @return
     */
    private int sumThrows(String value) {
        if (value == null || value.isEmpty() || value.equals("F")) {
            return 0;
        }
        return Integer.parseInt(value);
    }

    /**
     * validates if a value equals to 10 and return X
     * @param value
     * @return
     */
    private String ValidateIfTen(String value)
    {
        return  value.equals("10") ? "X" : value;
    }
}
