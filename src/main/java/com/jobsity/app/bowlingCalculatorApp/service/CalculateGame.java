package com.jobsity.app.bowlingCalculatorApp.service;

import com.jobsity.app.bowlingCalculatorApp.model.object.Player;
import com.jobsity.app.bowlingCalculatorApp.model.object.RawScore;

import java.util.List;
import java.util.Set;

public interface CalculateGame {
    <T extends Player> Set<T> calculateDataByPlayer(List<RawScore> scores);
}
