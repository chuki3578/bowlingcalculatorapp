package com.jobsity.app.bowlingCalculatorApp.service;

import com.jobsity.app.bowlingCalculatorApp.model.object.Player;

import java.util.Set;

public interface GameResults {
    StringBuilder displayResults(Set<Player> players);
}
