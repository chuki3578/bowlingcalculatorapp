package com.jobsity.app.bowlingCalculatorApp.service;

import com.jobsity.app.bowlingCalculatorApp.model.ReadDataFromFile;
import com.jobsity.app.bowlingCalculatorApp.model.object.Player;
import com.jobsity.app.bowlingCalculatorApp.model.object.RawScore;
import com.jobsity.app.bowlingCalculatorApp.model.object.Throw;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Creates DataStructure with scores for a game
 */
@Service
public class CalculateGameImpl implements CalculateGame {

    static final Logger logger = LogManager.getLogger(CalculateGameImpl.class);

    @Autowired
    private ReadDataFromFile readDataFromFile;

    private final String FAIL = "F";

    /**
     * Creates and calculate game user data(with throws per user)
     *
     * @return players list
     */
    @Override
    public <T extends Player> Set calculateDataByPlayer(List<RawScore> scores) {
        Set<Player> players = new HashSet<>();
        logger.info("Creating player data...");

        if (scores == null) {
            logger.info("none data to calculate!");
            return players;
        }

        try {

            for (RawScore score : scores) {
                Player player = addNewPlayer(score.getName());
                List<String> playerValues = score.getScores();
                int scoreSize = 0;

                for (int i = 0; i < playerValues.size(); i++) {
                    if (scoreSize < 10) {
                        Throw throwObject = new Throw();
                        String value = playerValues.get(i);
                        scoreSize = player.getThrowList().size();

                        if (scoreSize == 9) {
                            throwObject.setFirstThrow(value);
                            throwObject.setSecondThrow(i + 1 < playerValues.size() ? playerValues.get(i + 1) : null);
                            throwObject.setThirdThrow(i + 2 < playerValues.size() ? playerValues.get(i + 2) : null);
                            player.getThrowList().get(player.getThrowList().size() - 1).setNext(throwObject);
                            player.getThrowList().add(throwObject);
                            break;
                        } else if (value.equals("10")) {
                            throwObject.setFirstThrow(value);
                        } else {
                            throwObject.setFirstThrow(value);
                            throwObject.setSecondThrow(i+1 < playerValues.size() ? playerValues.get(i + 1) : "0");
                            i++;
                        }

                        if (scoreSize > 0) {
                            player.getThrowList().get(player.getThrowList().size() - 1).setNext(throwObject);
                        }
                        player.getThrowList().add(throwObject);
                    }
                }
                players.add(player);
            }
        } catch (Exception e) {
            logger.error("An unexpected error happened while calculating data");
            logger.error(e.getMessage());
        }
        return calculateScoreByUser(players);
    }

    /**
     * Calculates score by player
     *
     * @param players
     * @param <T>
     * @return players with calculated scores in a linked list
     */
    private <T extends Player> Set<T> calculateScoreByUser(Set<T> players) {
        logger.info("Calculating scores for each player...");
        ArrayList<String> al = new ArrayList<String>();

        for (Player player : players) {
            int totalScore = 0;

            for (Throw current : player.getThrowList()) {
                totalScore = calculateTotalScoreByThrow(current, totalScore);
                current.setTotalScore(totalScore);
            }
            player.setTotalScore(totalScore);
        }
        logger.info("done!");
        return players;
    }

    /**
     * Calculates totalScore for each throw
     *
     * @param current
     * @param totalScore
     * @return Throw with calculated score
     */
    private int calculateTotalScoreByThrow(Throw current, int totalScore) {
        Throw next = current.getNext();
        int nextFirstThrow = next != null ? getValueIfFail(next.getFirstThrow()) : 0;
        int nextSecondThrow = next != null ? getValueIfFail(next.getSecondThrow()) : 0;
        Throw afterNext = next != null ? next.getNext() : next;
        int first = getValueIfFail(current.getFirstThrow());
        int second = getValueIfFail(current.getSecondThrow());
        //las node in list
        if (next == null) {
            totalScore += first + second + getValueIfFail(current.getThirdThrow());
        } else if (first == 10) {
            totalScore += (nextFirstThrow != 10 || afterNext == null) ? first + nextFirstThrow + nextSecondThrow
                    : first + nextFirstThrow + getValueIfFail(afterNext.getFirstThrow());
        } else if (first + second == 10) {
            totalScore += first + second + nextFirstThrow;
        } else {
            totalScore += first + second;
        }
        return totalScore;
    }

    /**
     * Adds a new player
     *
     * @param name player
     * @return player object
     */
    private Player addNewPlayer(String name) {
        List<Throw> throwsList = new ArrayList<Throw>();
        Player player = new Player();
        player.setName(name);
        player.setThrowList(throwsList);
        return player;
    }

    /**
     * Returns 0 for F values and converts to int
     *
     * @param value
     * @return int value
     */
    private int getValueIfFail(String value) {
        if (value == null || value.equals(FAIL)) {
            return 0;
        }
        return Integer.parseInt(value);
    }


    public ReadDataFromFile getReadDataFromFile() {
        return readDataFromFile;
    }

    public void setReadDataFromFile(ReadDataFromFile readDataFromFile) {
        this.readDataFromFile = readDataFromFile;
    }
}
