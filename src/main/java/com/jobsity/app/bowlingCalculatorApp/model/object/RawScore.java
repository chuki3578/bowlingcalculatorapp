package com.jobsity.app.bowlingCalculatorApp.model.object;

import java.util.List;

public class RawScore {

    private String name;
    List<String> scores;

    public List<String> getScores() {
        return scores;
    }

    public void setScores(List<String> scores) {
        this.scores = scores;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

}
