package com.jobsity.app.bowlingCalculatorApp.model.object;

/**
 * Throw data
 */
public class Throw {

    private String firstThrow;
    private String secondThrow;
    private String thirdThrow;
    private int totalScore;
    private Throw next;

    public String getFirstThrow() {
        return firstThrow;
    }

    public void setFirstThrow(String firstThrow) {
        this.firstThrow = firstThrow;
    }

    public String getSecondThrow() {
        return secondThrow;
    }

    public void setSecondThrow(String secondThrow) {
        this.secondThrow = secondThrow;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public String getThirdThrow() {
        return thirdThrow;
    }

    public void setThirdThrow(String thirdThrow) {
        this.thirdThrow = thirdThrow;
    }

    public Throw getNext() {
        return next;
    }

    public void setNext(Throw next) {
        this.next = next;
    }

}
