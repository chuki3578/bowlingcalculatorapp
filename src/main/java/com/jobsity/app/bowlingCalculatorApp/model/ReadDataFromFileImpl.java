package com.jobsity.app.bowlingCalculatorApp.model;

import com.jobsity.app.bowlingCalculatorApp.model.object.RawScore;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Reads data from file
 */
@Repository
public class ReadDataFromFileImpl implements ReadDataFromFile {

    static final Logger logger = LogManager.getLogger(ReadDataFromFileImpl.class);
    private final String SEPARATOR = "\\t";
    private final String FAIL = "F";

    /**
     * Get data from file
     *
     * @param file file path
     * @return RawScore list
     */
    @Override
    public List<RawScore> getRawDataByPlayer(String file) {
        logger.info("Reading data from File....");
        List<RawScore> players = new ArrayList<RawScore>();

        if (!validateFile(file)) {
            logger.info("None file found!");
            return null;
        }

        try {
            List<String> allLines = Files.readAllLines(Paths.get(file));

            for (String score : allLines) {
                if (!StringUtils.isEmpty(score)) {
                    String[] values = score.split(SEPARATOR);
                    if (values.length == 2) {
                        if (players.stream().filter(x -> x.getName().equals(values[0])).count() == 0) {
                            players.add(createNewRawScore(values));
                        }

                        RawScore player = players.stream().filter(x -> x.getName().equals(values[0])).findFirst().get();
                        Optional<String> number = validateIfFailOrNumber(values[1]);
                        if (number.isPresent()) {
                            player.getScores().add(number.get());
                        } else {
                            logger.warn("Incorrect number or null value for this row: " + score);
                        }
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Error reading data from file : ");
            logger.error(e.getMessage());
        }

        return players;
    }

    /**
     * Validates if file exists, has corrrect extension or is empty
     * @param path
     * @return
     */
    private boolean validateFile(String path) {
        File file = new File(path);

        if(path == null || path.isEmpty()){
            return false;
        }
        if (!FilenameUtils.getExtension(path).equals("txt")) {
            logger.error("This file extension is not allowed. Only txt files!");
            return false;
        }
        if (!file.exists()) {
            logger.error("None file found on : "+path);
            return false;
        }
        if (file.length() == 0) {
            logger.error("File is empty!");
            return false;
        }

        return  true;
    }

    /**
     * Create a RawScoreObject
     *
     * @param values
     * @return
     */
    private RawScore createNewRawScore(String[] values) {
        RawScore rs = new RawScore();
        List<String> valuesList = new ArrayList<>();
        rs.setScores(valuesList);
        rs.setName(values[0]);
        return rs;
    }

    /**
     * validates if value is a number
     *
     * @param number
     * @return Optional value
     */
    private Optional<String> validateIfFailOrNumber(String number) {
        Optional<String> n = Optional.ofNullable(null);
        if (number.equals(FAIL)) {
            n = Optional.of(FAIL);
        }else if (number.isEmpty()) {
            n = Optional.of(String.valueOf("0"));
        }
        else if (number.matches("\\d+")) {
            int num = Integer.parseInt(number);
            num = (num > 10 || num < 0) ? 0 : num;
            n = Optional.of(String.valueOf(num));
        }
        return n;
    }
}
