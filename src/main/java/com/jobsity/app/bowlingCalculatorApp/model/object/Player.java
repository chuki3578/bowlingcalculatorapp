package com.jobsity.app.bowlingCalculatorApp.model.object;

import java.util.List;

/**
 * Player data
 */
public class Player {

    private String name;
    private List<Throw> throwList;
    private int totalScore;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Throw> getThrowList() {
        return throwList;
    }

    public void setThrowList(List<Throw> throwList) {
        this.throwList = throwList;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

}
