package com.jobsity.app.bowlingCalculatorApp.model;

import com.jobsity.app.bowlingCalculatorApp.model.object.RawScore;

import java.util.List;

public interface ReadDataFromFile {
    List<RawScore> getRawDataByPlayer(String file);
}
