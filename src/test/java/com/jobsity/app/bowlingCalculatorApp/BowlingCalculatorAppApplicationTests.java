package com.jobsity.app.bowlingCalculatorApp;

import com.jobsity.app.bowlingCalculatorApp.Utils.PlayerMockData;
import com.jobsity.app.bowlingCalculatorApp.model.ReadDataFromFileImpl;
import com.jobsity.app.bowlingCalculatorApp.model.object.Player;
import com.jobsity.app.bowlingCalculatorApp.service.CalculateGameImpl;
import com.jobsity.app.bowlingCalculatorApp.service.GameResultsImpl;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.rule.OutputCapture;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)

class BowlingCalculatorAppApplicationTests {

	@Mock
	ReadDataFromFileImpl readDataFromFile;

	@Mock
	GameResultsImpl gameResults;

	@Mock
	CalculateGameImpl calculateGame;

	@InjectMocks
	BowlingCalculatorAppApplication bowlingCalculatorAppApplication;

	@Test
	void contextLoads() throws Exception {
		Set<Player> players = PlayerMockData.getPlayers();
		StringBuilder sb = new StringBuilder();
		sb.append("testData");
		when(readDataFromFile.getRawDataByPlayer("")).thenReturn(PlayerMockData.createMockDataTwoPlayers());
		when(calculateGame.calculateDataByPlayer(PlayerMockData.createMockDataTwoPlayers())).thenReturn(players);
		when(gameResults.displayResults(players)).thenReturn(sb);
		bowlingCalculatorAppApplication.run();
	}

}
