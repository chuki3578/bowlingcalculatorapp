package com.jobsity.app.bowlingCalculatorApp.Utils;

import com.jobsity.app.bowlingCalculatorApp.model.object.Player;
import com.jobsity.app.bowlingCalculatorApp.model.object.RawScore;
import com.jobsity.app.bowlingCalculatorApp.model.object.Throw;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PlayerMockData {

    public static Set<Player> getPlayers(){
        Player player1 = getFisrtPlayer();
        Player player2 = getSecondPlayer();

        Set<Player> players = new HashSet<>();
        players.add(player1);
        players.add(player2);

        return players;
    }

    public static Set<Player> getPlayerWithZeroOrFail(String value) {
        List<Throw> thrList = new ArrayList<>();
        Set<Player> players = new HashSet<>();
        Player player1 = new Player();
        player1.setName("John");

        player1.setTotalScore((value.equals("0") || value.equals("F")) ? 0 : 300);

        for (int i = 0; i < 12; i++){
            Throw th = new Throw();
            th.setFirstThrow(value);
            thrList.add(th);
        }

        player1.setThrowList(thrList);
        players.add(player1);

        return players;
    }

    private static Player getFisrtPlayer(){
        List<Throw> thrList = new ArrayList<>();
        Player player1 = new Player();
        player1.setName("John");
        player1.setTotalScore(151);

        Throw thr = new Throw();
        thr.setTotalScore(16);
        thr.setFirstThrow("3");
        thr.setSecondThrow("7");

        Throw thr1 = new Throw();
        thr1.setTotalScore(25);
        thr1.setFirstThrow("6");
        thr1.setSecondThrow("3");
        thr.setNext(thr1);

        Throw thr2= new Throw();
        thr2.setTotalScore(44);
        thr2.setFirstThrow("10");
        thr1.setNext(thr2);

        Throw thr3 = new Throw();
        thr3.setTotalScore(53);
        thr3.setFirstThrow("8");
        thr3.setSecondThrow("1");
        thr2.setNext(thr3);

        Throw thr4 = new Throw();
        thr4.setTotalScore(82);
        thr4.setFirstThrow("10");
        thr3.setNext(thr4);

        Throw thr5 = new Throw();
        thr5.setTotalScore(101);
        thr5.setFirstThrow("10");
        thr5.setNext(thr4);

        Throw thr6 = new Throw();
        thr6.setTotalScore(110);
        thr6.setFirstThrow("9");
        thr6.setSecondThrow("0");
        thr5.setNext(thr6);

        Throw thr7 = new Throw();
        thr7.setTotalScore(124);
        thr7.setFirstThrow("7");
        thr7.setSecondThrow("3");
        thr6.setNext(thr7);

        Throw thr8 = new Throw();
        thr8.setTotalScore(132);
        thr8.setFirstThrow("4");
        thr8.setSecondThrow("4");
        thr7.setNext(thr8);

        Throw thr9 = new Throw();
        thr9.setTotalScore(151);
        thr9.setFirstThrow("10");
        thr9.setSecondThrow("9");
        thr9.setThirdThrow("0");
        thr8.setNext(thr9);

        thrList.add(thr);
        thrList.add(thr1);
        thrList.add(thr2);
        thrList.add(thr3);
        thrList.add(thr4);
        thrList.add(thr5);
        thrList.add(thr6);
        thrList.add(thr7);
        thrList.add(thr8);
        thrList.add(thr9);

        player1.setThrowList(thrList);
        return player1;
    }

    private static Player getSecondPlayer(){
        List<Throw> thrList = new ArrayList<>();
        Player player1 = new Player();
        player1.setName("Jeff");
        player1.setTotalScore(167);

        Throw thr = new Throw();
        thr.setTotalScore(20);
        thr.setFirstThrow("10");

        Throw thr1 = new Throw();
        thr1.setTotalScore(39);
        thr1.setFirstThrow("7");
        thr1.setSecondThrow("3");
        thr.setNext(thr1);

        Throw thr2= new Throw();
        thr2.setTotalScore(48);
        thr2.setFirstThrow("9");
        thr2.setSecondThrow("0");
        thr1.setNext(thr2);

        Throw thr3 = new Throw();
        thr3.setTotalScore(66);
        thr3.setFirstThrow("10");
        thr2.setNext(thr3);

        Throw thr4 = new Throw();
        thr4.setTotalScore(74);
        thr4.setFirstThrow("0");
        thr4.setSecondThrow("8");
        thr3.setNext(thr4);

        Throw thr5 = new Throw();
        thr5.setTotalScore(84);
        thr5.setFirstThrow("8");
        thr5.setSecondThrow("2");
        thr5.setNext(thr4);

        Throw thr6 = new Throw();
        thr6.setTotalScore(90);
        thr6.setFirstThrow("F");
        thr6.setSecondThrow("6");
        thr5.setNext(thr6);

        Throw thr7 = new Throw();
        thr7.setTotalScore(120);
        thr7.setFirstThrow("10");
        thr6.setNext(thr7);

        Throw thr8 = new Throw();
        thr8.setTotalScore(148);
        thr8.setFirstThrow("10");
        thr7.setNext(thr8);

        Throw thr9 = new Throw();
        thr9.setTotalScore(167);
        thr9.setFirstThrow("10");
        thr9.setSecondThrow("8");
        thr9.setThirdThrow("1");
        thr8.setNext(thr9);

        thrList.add(thr);
        thrList.add(thr1);
        thrList.add(thr2);
        thrList.add(thr3);
        thrList.add(thr4);
        thrList.add(thr5);
        thrList.add(thr6);
        thrList.add(thr7);
        thrList.add(thr8);
        thrList.add(thr9);

        player1.setThrowList(thrList);
        return player1;
    }

    public static List<RawScore> createMockData(String number) {
        List<RawScore> rsl = new ArrayList<>();
        RawScore rs = new RawScore();
        rs.setName("testPlayer");
        List<String> score = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            score.add(number);
        }

        rs.setScores(score);
        rsl.add(rs);
        return rsl;
    }

    public static List<RawScore> createMockDataTwoPlayers()
    {
        List<RawScore> rsl = new ArrayList<>();
        List<String> rsp1 = new ArrayList<>();
        List<String> rsp2 = new ArrayList<>();
        RawScore player1 = new RawScore();
        player1.setName("John");

        rsp1.add("3");
        rsp1.add("7");
        rsp1.add("6");
        rsp1.add("3");
        rsp1.add("10");
        rsp1.add("8");
        rsp1.add("1");
        rsp1.add("10");
        rsp1.add("10");
        rsp1.add("9");
        rsp1.add("0");
        rsp1.add("7");
        rsp1.add("3");
        rsp1.add("4");
        rsp1.add("4");
        rsp1.add("10");
        rsp1.add("9");
        rsp1.add("0");

        RawScore player2 = new RawScore();
        player2.setName("Jeff");

        rsp2.add("10");
        rsp2.add("7");
        rsp2.add("3");
        rsp2.add("9");
        rsp2.add("0");
        rsp2.add("10");
        rsp2.add("0");
        rsp2.add("8");
        rsp2.add("8");
        rsp2.add("2");
        rsp2.add("F");
        rsp2.add("6");
        rsp2.add("10");
        rsp2.add("10");
        rsp2.add("10");
        rsp2.add("8");
        rsp2.add("1");

        player1.setScores(rsp1);
        player2.setScores(rsp2);

        rsl.add(player1);
        rsl.add(player2);

        return rsl;
    }

}
