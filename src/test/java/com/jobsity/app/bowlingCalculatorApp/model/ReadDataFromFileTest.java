package com.jobsity.app.bowlingCalculatorApp.model;

import com.jobsity.app.bowlingCalculatorApp.model.object.RawScore;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class ReadDataFromFileTest {

    private ReadDataFromFileImpl readDataFromFile = new ReadDataFromFileImpl();


    /**
     * Check if file exists
     */
    @Test
    public void ReadFileTest() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(classLoader.getResource("game2.txt").getFile());
        assertTrue(file.exists());
    }

    /**
     * check if an user is loaded from file
     */
    @Test
    public void getRawDataByPlayerTest() {
        List<RawScore>  rs = readDataFromFile.getRawDataByPlayer("src/test/resources/game.txt");
        ClassLoader classLoader = this.getClass().getClassLoader();
        assertTrue(rs.stream().filter(x->x.getName().equals("John")).count() > 0);
        assertTrue(!rs.isEmpty());
    }

}
