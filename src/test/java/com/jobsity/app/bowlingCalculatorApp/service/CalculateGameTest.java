package com.jobsity.app.bowlingCalculatorApp.service;

import com.jobsity.app.bowlingCalculatorApp.Utils.PlayerMockData;
import com.jobsity.app.bowlingCalculatorApp.model.ReadDataFromFileImpl;
import com.jobsity.app.bowlingCalculatorApp.model.object.Player;
import com.jobsity.app.bowlingCalculatorApp.model.object.RawScore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.List;
import java.util.Set;

import static junit.framework.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class CalculateGameTest {

    @Mock
    ReadDataFromFileImpl readDataFromFile;

    @InjectMocks
    CalculateGameImpl calculateGame;

    /**
     * Validates when is a perfect game
     */
    @Test
    public void calculateDataByPlayerTest() {
        List<RawScore> rsl = PlayerMockData.createMockData("10");
        Player player = (Player) calculateGame.calculateDataByPlayer(rsl).stream().findFirst().get();
        assertTrue(player.getTotalScore() == 300);
    }

    /**
     * Validates when a game is Fail in all rows
     */
    @Test
    public void calculateDataByPlayerFailTest() {
        List<RawScore> rsl = PlayerMockData.createMockData("F");
        Player player = (Player) calculateGame.calculateDataByPlayer(rsl).stream().findFirst().get();
        assertTrue(player.getTotalScore() == 0);
    }

    /**
     * Validates when all throws equals zero
     */
    @Test
    public void calculateDataByPlayerZeroTest() {
        List<RawScore> rsl = PlayerMockData.createMockData("0");
        Player player = (Player) calculateGame.calculateDataByPlayer(rsl).stream().findFirst().get();
        assertTrue(player.getTotalScore() == 0);
    }

    /**
     * Validates game data for 2 players
     */
    @Test
    public void calculateDatForTwoPlayers() {
        List<RawScore> rsl = PlayerMockData.createMockDataTwoPlayers();
        Set<Player> players = calculateGame.calculateDataByPlayer(rsl);
        Player player1 =  players.stream().filter(x->x.getName().equals("Jeff")).findFirst().get();
        Player player2 = players.stream().filter(x->x.getName().equals("John")).findFirst().get();

        assertTrue(players.size() == 2);
        assertTrue(player1.getTotalScore() == 167);
        assertTrue(player2.getTotalScore() == 151);
    }

}
