package com.jobsity.app.bowlingCalculatorApp.service;


import com.jobsity.app.bowlingCalculatorApp.Utils.PlayerMockData;
import com.jobsity.app.bowlingCalculatorApp.model.object.Player;
import com.jobsity.app.bowlingCalculatorApp.model.object.RawScore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Set;

import static junit.framework.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class GameResultsTest {

    @InjectMocks
    GameResultsImpl gameResults;

    /**
     * Validates if print data is created
     */
    @Test
    public void calculateDataByPlayerTest() {
        List<RawScore> rsl ;
        Set<Player> pl = PlayerMockData.getPlayers();
        StringBuilder sb = gameResults.displayResults(pl);
        assertTrue(sb.length() > 300);
    }



}

